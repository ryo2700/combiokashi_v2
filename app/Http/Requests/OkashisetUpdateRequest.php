<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OkashisetUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name1' => 'required',
            'name2' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name1.required' => '名前１は必須です',
            'name2.required' => '名前２は必須です',
        ];
    }
}
