<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\okashiset;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\OkashisetStoreRequest;
use App\Http\Requests\OkashisetUpdateRequest;

class OkashisetController extends Controller
{
    public function index()
    {
        $okashisets = Okashiset::orderby('updated_at', 'desc')->get();

        return view('okashiset/index', compact('okashisets'));
    }

    public function create()
    {
        return view('okashiset.create');
    }

    public function store(OkashisetStoreRequest $request)
    {
        $okashiset = new Okashiset;

        $okashiset->name1 = $request->input('name1');
        $okashiset->name2 = $request->input('name2');
        $okashiset->taste = $request->input('taste');
        $okashiset->price = $request->input('price');
        $okashiset->surprise = $request->input('surprise');
        $okashiset->comment = $request->input('comment');
        $okashiset->user_id = Auth::id();

        //画像アップロード
        if ($file = $request->img1) {
            $fileName1 = time() . $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName1);
        } else {
            $fileName1 = "";
        }
        if ($file = $request->img2) {
            $fileName2 = time() . $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName2);
        } else {
            $fileName2 = "";
        }
        $okashiset->img1 = $fileName1;
        $okashiset->img2 = $fileName2;

        $okashiset->save();

        $okashisets = Okashiset::all();
        return view('okashiset/index', compact('okashisets'));
    }

    public function show($id)
    {
        $okashiset = Okashiset::find($id);
        return view('okashiset.show', compact('okashiset'));
    }

    public function edit($id)
    {
        $okashiset = Okashiset::find($id);
        return view('okashiset.edit', compact('okashiset'));
    }

    public function update(OkashisetUpdateRequest $request, $id)
    {
        $okashiset = Okashiset::find($id);

        $okashiset->name1 = $request->input('name1');
        $okashiset->name2 = $request->input('name2');
        $okashiset->taste = $request->input('taste');
        $okashiset->price = $request->input('price');
        $okashiset->surprise = $request->input('surprise');
        $okashiset->comment = $request->input('comment');

        if ($file = $request->img1) {
            $fileName1 = $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName1);
            $okashiset->img1 = $fileName1;
        }
        if ($file = $request->img2) {
            $fileName2 = $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName2);
            $okashiset->img2 = $fileName2;
        }

        $okashiset->save();
        return view('okashiset.show', compact('okashiset'));
    }

    public function destroy($id)
    {
        $okashiset = Okashiset::find($id);
        $okashiset->delete();

        return redirect('/');
    }

    public function mypage()
    {
        $myOkashisets = Okashiset::orderby('updated_at', 'desc')->where('user_id', Auth::id())->get();
        return view('okashiset.mypage', compact('myOkashisets'));
    }
}
