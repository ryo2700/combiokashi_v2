<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ログインしていなくても、index,showは見れるように
Route::get('/', 'OkashisetController@index');
Route::get('show/{id}', 'OkashisetController@show')->name('okashiset.show');

//ログインしていないと、index,showしか見れないように
Route::group(['middleware' => 'auth'], function(){
    Route::get('create', 'OkashisetController@create')->name('okashiset.create');
    Route::post('store', 'OkashisetController@store')->name('okashiset.store');
    Route::get('edit/{id}', 'OkashisetController@edit')->name('okashiset.edit');
    Route::post('update/{id}', 'OkashisetController@update')->name('okashiset.update');
    Route::get('destroy/{id}', 'OkashisetController@destroy')->name('okashiset.destroy');

    Route::get('mypage', 'OkashisetController@mypage')->name('okashiset.mypage');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
