<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOkashisetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('okashisets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name1', 40)->nullable();
            $table->string('img1', 100);
            $table->string('name2', 40)->nullable();
            $table->string('img2', 100);
            $table->integer('taste');
            $table->integer('price');
            $table->integer('surprise');
            $table->text('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('okashisets');
    }
}
