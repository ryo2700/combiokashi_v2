@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">詳細</div>
        <div class="card-body">
          <div>
            <div>{{ $okashiset->id }}</div>
            <span>
              <img src="/uploads/{{ $okashiset->img1 }}" width="200px" height="200px">
              {{ $okashiset->name1 }}
            </span>
            <span>
              <img src="/uploads/{{ $okashiset->img2 }}" width="200px" height="200px">
              {{ $okashiset->name2 }}
            </span>
          </div>
          <div>
            <span>{{ $okashiset->taste }}</span>
            <span>{{ $okashiset->price }}</span>
            <span>{{ $okashiset->surprise }}</span>
          </div>
          <div>
            <span>{{ $okashiset->comment }}</span>
          </div>

          <!-- 自分の投稿のみ編集・削除ができる  -->
          @if ($okashiset->user_id == Auth::id())
            <div>
              <a href="{{ route('okashiset.edit', ['id' => $okashiset->id]) }}">編集</a>
            </div>
            <div>
              <a href="{{ route('okashiset.destroy', ['id' => $okashiset->id]) }}">削除</a>
            </div>
          @endif


        </div>
      </div>
    </div>
  </div>
</div>

<script>
/********************************
 * 削除ボタンを押してすぐにレコードが削除
 * されるのも問題なので、一旦javascriptで
 * 確認メッセージを流します。
 *********************************/
function deletePost(e) {
  'use strict';
  if (confirm('本当に削除していいですか？')) {
    document.getElementById('delete_' + e.dataset.id).submit();
  }
}
</script>

@endsection
