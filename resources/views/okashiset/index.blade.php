@extends('layouts.app')

@section('content')
<div class="container">

  <div id="regist-okashi" class="mb-2 pb-1 d-flex align-items-end justify-content-center">
    <a href="{{ route('okashiset.create') }}" class="btn btn-success btn-lg">
      <i class="fas fa-coffee pr-2"></i>おかしセット投稿
    </a>
  </div>

  <div class="row justify-content-center">

    <div id="new-post" class="col-md-8">
      <div class="card">
        <div class="card-header">最新投稿</div>

          <div class="card-body">
              @foreach($okashisets as $okashiset)
                  <div class="card">
                    <div class="card-body">
                    <div class="row">
                      <div class="col-md-6 row post-okashiset-image">
                        <div class="col-6">
                          <img src="/uploads/{{ $okashiset->img1 }}" class="w-100 h-75">
                          <div class="text-center">{{ $okashiset->name1 }}</div>
                        </div>
                        <div class="col-6">
                          <img src="/uploads/{{ $okashiset->img2 }}" class="w-100 h-75">
                          <div class="text-center">{{ $okashiset->name2 }}</div>
                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="row">
                            <div class="col-4">
                             <div class="text-center">味</div>
                             <div>{{ $okashiset->taste }}</div>
                            </div>
                            <div class="col-4">
                              <div class="text-center">価格</div>
                              <div>{{ $okashiset->price }}</div>
                            </div>
                            <div class="col-4">
                              <div class="text-center">驚き</div>
                              <div>{{ $okashiset->surprise }}</div>
                            </div>
                          </div>
                          <div class="row">
                            <!-- <div class="card w-100"> -->
                              <div class="">{{ $okashiset->comment }}</div>
                            <!-- </div> -->
                          </div>
                          <div class="row d-flex align-items-end justify-content-end">
                            <a href="{{ route('okashiset.show', [ 'id' => $okashiset->id ]) }}">詳細</a>
                            {{ $okashiset->updated_at->format('n/j G:i') }}
                            {{ $okashiset->user->name }}
                          </div>
                      </div>
                    </div>
                    </div>
                  </div>
              @endforeach
          </div>
      </div>
    </div>

    <div id="total-lanking-post" class="col-md-4">
      <div class="card">
        <div class="card-header">総合 ランキングBest5</div>
        <div class="card-body">
        </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
